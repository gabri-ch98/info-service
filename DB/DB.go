package DB

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"log"
	_ "text/scanner"
)

type Connection struct {
	Conn *sql.DB
	Err  error
}

type Car struct {
	Ident int `json:"id,omitempty"`
	Brand string `json:"brand,omitempty"`
	Model string `json:"model,omitempty"`
	Horses string `json:"horse_power,omitempty"`
}

type Carful struct {
	Ident int `json:"id,omitempty"`
	Brand string `json:"brand,omitempty"`
	Model string `json:"model,omitempty"`
	Horses int `json:"horse_power,omitempty"`
}

func Connect() *sql.DB{
	conn, err := sql.Open("mysql", "gabri:facil123@tcp(172.16.1.3:3306)/myDB")
	if err != nil {
		log.Println(err)
	}
	return conn
}

func  LastID(q Connection) (int, error){
	var id int
	err := q.Conn.QueryRow("select MAX(id) from cars").Scan(&id)
	if err != nil {
		log.Println(err)
	}
	return id, err
}

func RegCar(q Connection, c Car) error{
	_, err := q.Conn.Query("insert into cars(id,brand,model,horse_power) values(?,?,?,?)", c.Ident, c.Brand, c.Model, c.Horses)
	if err != nil {
		log.Println(err)
	}
	return err
}

func ListCar(q Connection, id int) (Carful, error){
	var c Carful
	err := q.Conn.QueryRow("select * from cars where id = ?", id).Scan(&c.Ident, &c.Brand, &c.Model, &c.Horses)
	return c, err
}