package main
import (
	"MicroServicies/DB"
	_ "MicroServicies/DB"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
	"strconv"
	_ "text/scanner"
)

type Carful struct {
	Ident int `json:"id,omitempty"`
	Brand string `json:"brand,omitempty"`
	Model string `json:"model,omitempty"`
	Horses int `json:"horse_power,omitempty"`
}

var er error

func main() {
	var con DB.Connection
	//con.Conn = DB.Connect()
	//defer con.Conn.Close()

	http.HandleFunc("/service/v1/cars/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == "POST"{
			fmt.Fprint(w,"This service only accepts GET method")
		}else {
			con.Conn = DB.Connect()
			defer con.Conn.Close()
			var car DB.Carful
			id := r.URL.Path[len("/service/v1/cars/"):]
			i, _ := strconv.Atoi(id)

			car, er = DB.ListCar(con,i)
			if er != nil {
				if er.Error() == "sql: no rows in result set" {
					fmt.Fprint(w, "This car is not registered yet.")
				}else {
					fmt.Fprint(w, "An error related to the DB has occurred, please try to list your car again.")
				}
			} else {
				json.NewEncoder(w).Encode(car)
			}
		}
	})
	http.ListenAndServe(":8080", nil)
}
