# syntax=docker/dockerfile:1

FROM golang:1.16.3
# -alpine
WORKDIR /MicroServices

COPY go.mod ./
COPY go.sum ./
COPY ms-main.go ./
#COPY msPOST ./
COPY DB/ /usr/local/go/src/MicroServicies/DB
RUN go mod download

RUN go build ms-main.go

CMD [ "./ms-main" ]
